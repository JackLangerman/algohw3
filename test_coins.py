def step(S):                           
    for i in range(1, len(S)):
        S[i] += 1                      
        S[0] -= 1                
        if S[0] <= 0:                  
            S.sort(reverse=True)
            return             


def decending(S):                             
    for i in range(1, len(S)):
        if S[i-1] != 0 and S[i-1] - S[i] != 1:
            return False         
        elif S[i-1] == 0 and S[i] != 0:
            return False   
    return True                



def simulate(N):
    for h in range(N):
        n = h*(h+1)//2
        S = [1 for _ in range(n)]
        print(n, h, "|", S)
        i = 1
        while not decending(S):
            step(S)
            print("\t{}: ".format(i), S)
            i += 1


if __name__ == '__main__':
	simulate(10)